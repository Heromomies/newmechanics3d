﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSphere : MonoBehaviour
{
    [SerializeField, Range(0f, 100f)]
    float maxSpeed = 10f;
    [SerializeField, Range(0f, 100f)]
    float maxAcceleration = 10f;
    [SerializeField, Range(0f, 10f)]
    float jumpHeight = 2f;
    [SerializeField, Range(0, 5)]
    int maxAirJumps = 0, maxAirAcceleration = 1;
    [SerializeField, Range(0f, 90f)]
    float maxGroundAngle = 25f;
    [SerializeField, Range(0f, 100f)]
    float maxSnapSpeed = 100f;
    [SerializeField, Min(0f)]
    float probeDistance = 1f;
    [SerializeField]
    LayerMask probeMask = -1;
    
    private Vector3 _velocity, _desiredVelocity;
    private Rigidbody _body;
    private bool _desiredJump;
    private bool _onGround;
    private int _jumpPhase;
    private float _minGroundDotProduct;
    private Vector3 _contactNormal;
    private int _groundContactCount;
    private int _stepsSinceLastGrounded;
    private bool OnGround => _groundContactCount > 0;
    void Awake () {
        _body = GetComponent<Rigidbody>();
        OnValidate();
    }
    void OnValidate () {
        _minGroundDotProduct = Mathf.Cos(maxGroundAngle * Mathf.Deg2Rad);
    }
    void Update () {
        Vector2 playerInput;
        playerInput.x = Input.GetAxis("Horizontal");
        playerInput.y = Input.GetAxis("Vertical");
        playerInput = Vector2.ClampMagnitude(playerInput, 1f);
        Vector3 acceleration = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        _desiredVelocity = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        _desiredJump |= Input.GetButtonDown("Jump");
    }
    void FixedUpdate () {
        UpdateState();
        AdjustVelocity();
        _body.velocity = _velocity;
        ClearState();
        
        if (_desiredJump) {
            _desiredJump = false;
            Jump();
        }
    }
    void ClearState () {
        _groundContactCount = 0;
        _contactNormal = Vector3.zero;
    }
    void UpdateState () {
        _velocity = _body.velocity;
        _stepsSinceLastGrounded += 1;
        if (OnGround || SnapToGround()) {
            _stepsSinceLastGrounded = 0;
            _jumpPhase = 0;
            if (_groundContactCount > 1) {
                _contactNormal.Normalize();
            }
        }
    }
    void Jump() {
        if (_onGround || _jumpPhase < maxAirJumps) {
            _jumpPhase += 1;
            float jumpSpeed = Mathf.Sqrt(-2f * Physics.gravity.y * jumpHeight);
            float alignedSpeed = Vector3.Dot(_velocity, _contactNormal);
            if (alignedSpeed > 0f) {
                jumpSpeed = Mathf.Max(jumpSpeed - alignedSpeed, 0f);
            }
            _velocity += _contactNormal * jumpSpeed;
        }
    }
    void OnCollisionEnter (Collision collision) {
        EvaluateCollision(collision);
    }

    void OnCollisionStay (Collision collision) {
        EvaluateCollision(collision);
    }
    Vector3 ProjectOnContactPlane (Vector3 vector) {
        return vector - _contactNormal * Vector3.Dot(vector, _contactNormal);
    }
    void AdjustVelocity () {
        Vector3 xAxis = ProjectOnContactPlane(Vector3.right).normalized;
        Vector3 zAxis = ProjectOnContactPlane(Vector3.forward).normalized;
        
        float currentX = Vector3.Dot(_velocity, xAxis);
        float currentZ = Vector3.Dot(_velocity, zAxis);
        
        float acceleration = _onGround ? maxAcceleration : maxAirAcceleration;
        float maxSpeedChange = acceleration * Time.deltaTime;

        float newX = Mathf.MoveTowards(currentX, _desiredVelocity.x, maxSpeedChange);
        float newZ = Mathf.MoveTowards(currentZ, _desiredVelocity.z, maxSpeedChange);
        
        _velocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);
    }
    void EvaluateCollision(Collision collision)
    {
        for (int i = 0; i < collision.contactCount; i++) {
            Vector3 normal = collision.GetContact(i).normal;
            _onGround |= normal.y >=  _minGroundDotProduct;
            if (normal.y >= _minGroundDotProduct) {
                _groundContactCount += 1;
                _contactNormal += normal;
            }
            else {
                _contactNormal = Vector3.up;
            }
        }
    }
    bool SnapToGround () {
        if (_stepsSinceLastGrounded > 1) {
            return false;
        }
        float speed = _velocity.magnitude;
        if (speed > maxSnapSpeed) {
            return false;
        }
        if (!Physics.Raycast(_body.position, Vector3.down, out RaycastHit hit, probeDistance)) {
            return false;
        }
        if (hit.normal.y < _minGroundDotProduct) {
            return false;
        }

        _groundContactCount = 1;
        _contactNormal = hit.normal;
        //float speed = velocity.magnitude;
        float dot = Vector3.Dot(_velocity, hit.normal);
        if (dot > 0f) {
            _velocity = (_velocity - hit.normal * dot).normalized * speed;
        }
        return false;
    }
}
